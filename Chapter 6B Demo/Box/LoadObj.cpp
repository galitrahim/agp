#include "LoadObj.h"
#include <sstream>
#include "GeometryGenerator.h"
#include "MathHelper.h"

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}


vector<Vertex> vertices;
vector<Triangle> triangles;

void LoadObj::LoadObjFromFile(string filename)
{
	ifstream filestream;
	filestream.open(filename);

	while (!filestream.eof())
	{
		std::string line;
		std::getline(filestream, line);
		vector<std::string> splittedline = split(line, ' ');
		if (splittedline.size() == 0)
			continue;

		string s = splittedline.at(0);

			if (s == "v")
			{
				Vertex p;
				p.Pos = XMFLOAT3(std::stod(splittedline.at(1)), std::stod(splittedline.at(2)), std::stod(splittedline.at(3)));
				p.Color = (const float*)&Colors::Red;
				vertices.push_back(p);
			}

			if (s == "f")
			{
				Triangle t;
				t.points[0] = atoi(split(splittedline.at(1), '/').at(0).c_str()) - 1;
				t.points[1] = atoi(split(splittedline.at(2), '/').at(0).c_str()) - 1;
				t.points[2] = atoi(split(splittedline.at(3), '/').at(0).c_str()) - 1;
				triangles.push_back(t);

				if (splittedline.size() > 4)
				{
					Triangle t2;
					t2.points[0] = atoi(split(splittedline.at(3), '/').at(0).c_str()) - 1;
					t2.points[1] = atoi(split(splittedline.at(4), '/').at(0).c_str()) - 1;
					t2.points[2] = atoi(split(splittedline.at(1), '/').at(0).c_str()) - 1;
					triangles.push_back(t2);
				}

			}
	}

	
	filestream.close();

	// Teken
};