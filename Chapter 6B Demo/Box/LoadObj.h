#pragma once

using namespace std;
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Point3.h"
#include "d3dApp.h"
#include "d3dx11Effect.h"
#include "GeometryGenerator.h"
#include "MathHelper.h"

struct Vertex
{
	XMFLOAT3 Pos;
	XMFLOAT4 Color;
};

class LoadObj
{
public:
	vector<Vertex> vertices;
	vector<Triangle> triangles;

	void LoadObjFromFile(string fileName);
};

