#include "Assignment6.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
	PSTR cmdLine, int showCmd)
{
	Assignment6 Shading(hInstance);

	if (!Shading.Init())
	{
		return EXIT_FAILURE;
	}

	Shading.Run();

}
